
package maptrail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Class MapTrail - represents Main Application Class for MapChallange game
 *
 *@author     Peter Jura
 *@version    1.0.0
 */
public class MapTrail extends Application {
    
    public Data data = new Data();
    private ArrayList<Integer> randInts;
    private int[] colors = new int[44];
    private int level = 1;
    private int success = 0;
    private int fail = 0;
    private GridPane gridPane = new GridPane();
    private Group group = new Group();
    private ArrayList<SVGPath> map = new ArrayList<SVGPath>();
    private Text text2 = new Text(); 
    private Text text1 = new Text();
    private Text text3 = new Text();
    private ImageView imageView = new ImageView();
    private Stage primaryStage;
    
    @Override
public void start(Stage primaryStage) 
{
    loadRandomInts();
    
    imageView = new ImageView(new Image("/maptrail/images/"+data.getIDs()[randInts.get(level)].toLowerCase()+".png")); 
    imageView.setX(50); 
    imageView.setY(25); 
    imageView.setFitHeight(50); 
    imageView.setFitWidth(80);
    
    text3.setText("Find 10 random countries in Europe!"); 
    text2.setText("Score: " +success+ "/10"); 
    text1.setText("Click on: " + data.getNames()[randInts.get(level)]);
    text1.setStyle("-fx-font: normal bold 20px 'serif' ");
    text2.setStyle("-fx-font: normal bold 20px 'serif' ");
    text3.setStyle("-fx-font: normal bold 20px 'serif' ");
    
    gridPane.setMinSize(400, 200);    
    gridPane.setAlignment(Pos.TOP_CENTER);    
    gridPane.setStyle("-fx-background-color: BEIGE;"); 
    
    String[] paths = data.getPaths();
        
    for (int i = 0; i < paths.length; i++) {
        SVGPath path = new SVGPath();
        path.setContent(paths[i]);
        path.setFill(Color.BISQUE);
        path.setStroke(Color.CORNFLOWERBLUE);
        path.setStrokeWidth(1);
        int counter = i;
        final Tooltip t = new Tooltip(data.getNames()[counter]);
        path.setOnMouseEntered((event) -> {
            path.setFill(Color.GAINSBORO);
            double x = (path.localToScene(path.getBoundsInParent()).getMinX() +path.localToScene(path.getBoundsInParent()).getMaxX())/2 + primaryStage.getX();
            double y = path.localToScene(path.getBoundsInParent()).getMinY() + primaryStage.getY() - 10;
            if (colors[counter] == 1)
            {
                t.show(primaryStage,x,y);
            }
            else if(colors[counter] == 2)
            {
                t.show(primaryStage,x,y);
            }
        });
        path.setOnMouseExited((event) -> {
            if (colors[counter] == 1)
            {
                path.setFill(Color.FORESTGREEN);
                t.hide();
            }
            else if(colors[counter] == 2)
            {
                path.setFill(Color.CORAL);
                t.hide();
            }else{
                path.setFill(Color.BISQUE); 
            }
        });
        path.setOnMouseClicked((MouseEvent event) -> {
            if (counter == randInts.get(level))
            {
                path.setFill(Color.FORESTGREEN);
                colors[counter] = 1;
                Alert warning = new Alert(Alert.AlertType.INFORMATION, "", ButtonType.OK);
                warning.setHeaderText("Good! This is: "+ data.getNames()[counter]);
                warning.setTitle("Info");
                warning.showAndWait();
                success++;
            }else{
                map.get(randInts.get(level)).setFill(Color.CORAL);
                colors[randInts.get(level)]= 2;
                Alert warning = new Alert(Alert.AlertType.ERROR,"" , ButtonType.OK);
                warning.setHeaderText("Bad guess! This is: "+ data.getNames()[counter]);
                warning.setTitle("Info");
                warning.showAndWait();
                fail++;
            }
            level++;
            text2.setText("Score: " +success+ "/10");
            
            if (level > 10)
            {
               newGame();         
            }
            text1.setText("Find country: " + data.getNames()[randInts.get(level)]);
            imageView.setImage(new Image("/maptrail/images/"+data.getIDs()[randInts.get(level)].toLowerCase()+".png"));
        });
        map.add(path);
    }
    group.getChildren().addAll(map);
    
    MenuBar menuBar = new MenuBar();
    menuBar.prefWidthProperty().bind(primaryStage.widthProperty());
    
    Menu fileMenu = new Menu("Game");
    MenuItem novaHra = new MenuItem("New game");
    MenuItem koniecHry = new MenuItem("End Game");
    MenuItem napoveda = new MenuItem("Help");
    novaHra.setOnAction( t ->{
        newGame();
    });
    napoveda.setOnAction(t -> Help.display());
    koniecHry.setOnAction(t -> Platform.exit());
    fileMenu.getItems().addAll(novaHra,napoveda,new SeparatorMenuItem(), koniecHry);
    menuBar.getMenus().addAll(fileMenu);
    
    gridPane.setHgap(5);
    gridPane.setVgap(5);
    gridPane.setHalignment(imageView, HPos.RIGHT);
    gridPane.setHalignment(group, HPos.CENTER);
    gridPane.add(menuBar, 0, 0, 2, 1);
    gridPane.add(text1, 0, 2, 1, 1);
    gridPane.add(text3, 0, 1, 1, 1);
    gridPane.add(text2, 0, 3, 1, 1);
    gridPane.add(imageView,1, 2, 1, 1);
    gridPane.add(group,0, 4, 2, 1);
    

    Scene scene = new Scene(gridPane);
    this.primaryStage = primaryStage;
    primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
            Platform.exit();}
    });
    primaryStage.setTitle("MapTrail"); 
    primaryStage.setScene(scene);
    primaryStage.show();
}

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    /**
     * Method load list of random integers
     * to randomize order of guessed countries
     * 
     */
    public void loadRandomInts()
    {
        randInts = new ArrayList<>(44);
        for (int i = 0; i <= 43; i++){
            randInts.add(i);
        }
        Collections.shuffle(randInts);
    }
    
    /**
     * Method resets the colors upon new Game
     * 
     */
    public void resetColors()
    {
        map.forEach((path) -> {
            path.setFill(Color.BISQUE); 
        });
        colors = new int[44];
    }
    
    /**
     * Method starts a new game
     * 
     */
    public void newGame()
    {
        Alert warning = new Alert(Alert.AlertType.INFORMATION, "Wanna play again?" , ButtonType.YES,ButtonType.NO);
        warning.setHeaderText("Your Score: "+ success);
        warning.setTitle("Score");
        Optional<ButtonType> result = warning.showAndWait();
        if (result.get() == ButtonType.YES){
            level = 1;
            success = 0;
            fail = 0;
            loadRandomInts();
            text1.setText("Find country: " + data.getNames()[randInts.get(level)]);  
            text2.setText(("Score: " + success));
            imageView.setImage(new Image("/maptrail/images/"+data.getIDs()[randInts.get(level)].toLowerCase()+".png"));
            resetColors();
        } else {
            primaryStage.close();
        }
    }
}
