package maptrail;

import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * Class Help - represents help text
 *
 *@author     Peter Jura
 *@version    1.0.0
 */
public class Help {

    /**
     * methoda sluzi k zobrazeni napovedy
     * 
     */
    public static void display() {
        Stage window = new Stage();
        window.setTitle("Help");             

        WebView  browser = new WebView();
        WebEngine engine = browser.getEngine();
        String url = Help.class.getResource("napoveda.html").toExternalForm();
        engine.load(url);

        StackPane sp = new StackPane();
        sp.getChildren().add(browser);
        sp.setMaxSize(300, 200);

        Scene root = new Scene(sp);
        window.setScene(root);
        window.show();
    }

}